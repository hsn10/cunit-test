scalaVersion := "2.12.13"

val Versions = new {
    val CQLDriver = "4.11.1"
    val scalatest = "3.2.7"
    val java8compat = "1.0.0-RC1"
    val slf4j = "1.7.30"
}

// Cassandra driver
libraryDependencies ++= Seq(
   "com.datastax.oss" % "java-driver-core" % Versions.CQLDriver
)

libraryDependencies += "org.scalatest" %% "scalatest" % Versions.scalatest % "test"

libraryDependencies += "org.slf4j" % "slf4j-simple" % Versions.slf4j % "test"