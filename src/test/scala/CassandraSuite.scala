package com.filez.scala.sbt.cunit.test

import org.scalatest._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future,Await}
import scala.concurrent.duration.Duration

object CassandraSuite {
   val KEYSPACE = "keyspace1"
}

class CassandraSuite extends Suites() with BeforeAndAfterAll with EmbeddedCassandra {

   override
   def beforeAll() = {
      createKeyspace(CassandraSuite.KEYSPACE)
      session.execute(s"USE ${CassandraSuite.KEYSPACE}")
   }

   override
   def afterAll() = {
      session.close()
   }

   def createKeyspace(name: String) = {
      import com.datastax.oss.driver.api.core.CqlSession
      import java.net.InetSocketAddress

      val session = CqlSession.builder()
                      .addContactPoint(new InetSocketAddress("127.0.0.1", 9142))
                      .withLocalDatacenter("datacenter1")
                      .build()

      session.execute(s"""CREATE KEYSPACE IF NOT EXISTS $name WITH replication={'class':'SimpleStrategy','replication_factor':'1'} AND durable_writes=false""")
      session.close()
  }

   override val nestedSuites = Vector[Suite] (  new CreateTablesSpec(session) )
}
