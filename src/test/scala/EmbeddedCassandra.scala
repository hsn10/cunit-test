package com.filez.scala.sbt.cunit.test

import com.datastax.oss.driver.api.core.{CqlSession, CqlIdentifier}
import java.net.InetSocketAddress

trait EmbeddedCassandra extends AutoCloseable {
   lazy val session = CqlSession.builder()
                      .addContactPoint(new InetSocketAddress("127.0.0.1", 9142))
                      .withLocalDatacenter("datacenter1")
                      // .withKeyspace(CqlIdentifier.fromCql(CassandraSuite.KEYSPACE))
                      .build()

   override
   def close() = session.close()
}
