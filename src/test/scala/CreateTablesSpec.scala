package com.filez.scala.sbt.cunit.test

import org.scalatest._
import wordspec._

import com.datastax.oss.driver.api.core.{CqlSession, CqlIdentifier}

class CreateTablesSpec(session: CqlSession) extends AnyWordSpec {
    "Recipe database" should {
        "create cassandra tables" in {
            session.execute("DROP TABLE IF EXISTS recipes")
            session.execute("CREATE TABLE recipes (id smallint PRIMARY KEY, text varchar)")
        }
    }
}